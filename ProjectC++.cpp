﻿#include <iostream>
#include <string>
#include <stdint.h>
#include <iomanip>
#include <time.h>
#include <ctime>

class Animal
{
public:
	virtual void Voice() const = 0;
};
class Dog : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Woof!\n";
	}
};
class Cat : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Meow!\n";
	}
};
class Cow : public Animal
{
public:
	void Voice() const override
	{
		std::cout << "Mooo!\n";
	}
};
int main()
{
	Animal* Animals[3];
	Animals[0] = new Dog();
	Animals[1] = new Cat();
	Animals[2] = new Cow();
	for (Animal* a : Animals)
		a->Voice();
}